""" 
@author: Darlin KUAJO
Ce module contient les fonctionnalités utiles au prétraitement des données.
"""

import os
import nltk
import string
import numpy as np
import pandas as pd
from tqdm import tqdm

EN_STOPWORD = nltk.corpus.stopwords.words('english')

class MySentences(object):
    """Définition d'un itérateur convivial pour la mémoire"""
    
    def __init__(self, dirname):
        self.dirname = dirname

    def __iter__(self):
        for fname in os.listdir(self.dirname):
            for line in open(os.path.join(self.dirname, fname), 'r', encoding='UTF-8'):
                yield clear_description(line)

def remove_punctuation(texte):
    """ Cette fonction permet de supprimer les caractères de ponctuation contenus dans un texte
    
    texte : str
    
    Valeur de retour : str
    """
    
    result = "".join( [ch for ch in texte if ch not in string.punctuation])
    
    return result

def tokenizer(texte):
    """ Cette fonction permet de tokéniser un texte 
    
    texte : str
    
    Valeur de retour : list(str)
    """
    
    words = texte.split()
    
    return words

def remove_stopwords(tokens_list):
    """ Cette fonction permet de supprimer les mots vides de la langue anglaise contenus dans une liste de tokens 
    
    tokens_list : list(str)
     
     Valeur de retour : list(str)
    """
    
    result = [word for word in tokens_list if word not in EN_STOPWORD]
    
    return result

def clear_description(description):
    """Cette fonction permet de prétraiter la description textuelle d'un produit
    
    Les opérations de prétraitement sont:
    - Conversion de la description en minuscule
    - Suppression des caractères de ponctuation
    - Tokénisation
    - Suppression des mots vides
    
    description : str
    
    Valeur de retour : list(str)
    """
    
    description = description.lower()
    description = remove_punctuation(description)
    description = tokenizer(description)
    description = remove_stopwords(description)
    
    return description